<?php
include("fonction/donneeTraiter.php");
include("Donnees.inc.php");

session_start();
if (isset($_POST['submit'])){
	
	$ClassUsername='ok';
	$ClassNom='ok';
	$ClassPrenom='ok';
	$ClassPassword='ok';
	$ClassRepass='ok';
	$ClassSexe='ok';
    $ClassVille='ok';
	$ClassAdresse='ok';
	$ClassEmail='ok';
	$ClassPostal='ok';
	$ClassTele='ok';
	$ClassAdresse='ok';
	$ClassNaissance='ok';
	
	$toutvabien='ok';//si la formule est bien forme ,=ok
	
	$ChampIncorrectUsername='';
	$ChampIncorrectNom='';
	$ChampIncorrectPrenom='';
	$ChampIncorrectPassword='';
	$ChampIncorrectRepass='';
	$ChampIncorrectSexe='';
	$ChampIncorrectVille='';
	$ChampIncorrectAdresse='';
	$ChampIncorrectEmail='';
	$ChampIncorrectPostal='';
	$ChampIncorrectTele='';
	$ChampIncorrectNaissance='';
	
	
	
	 //nom
	
	if(!isset($_POST['nom']) || $_POST['nom']==''){
		     
		$ChampIncorrectNom='Le nom n\'est pas positionnée';
		$ClassNom='error';
	}
	else{
		
		$_SESSION[$_SESSION['usr']['name']]['nom']=$_POST['nom'];
		
		
	}
	
	 //prenom
	
	if(!isset($_POST['prenom']) || $_POST['prenom']==''){
		     $ChampIncorrectNom='Le prenom n\'est pas positionnée';
		     $ClassNom='error';
	}
	else{
		
		$_SESSION[$_SESSION['usr']['name']]['prenom']=$_POST['prenom'];
		
		
	}
	//sexe
	if(  (!isset($_POST['sexe'])) 			// la variable sexe n'est pas positionnée
	   ||(  (trim($_POST['sexe'])!='f')     // sexe ne vaut pas 'f'
	      &&(trim($_POST['sexe'])!='h')     // ni 'h' 
		 )
	  )
      { 
		  $ChampIncorrectSexe='Sex n\'est pas positionnée';
	      $ClassSexe='error';

	  }
	else{//la variable sexe est positionnee ,et bien = h ou f
		
	$_SESSION[$_SESSION['usr']['name']]['sexe']=$_POST['sexe'];
		
}
	
	//adresse 
	if(  (!isset($_POST['adresse'])) 		// la variable adresse n'est pas positionnée
       ||(trim($_POST['adresse'])=='')    // adresse vide
	  )
	  { 
		 
		  $ChampIncorrectAdresse='L\'adresse n\'est pas positionnée';
	      $ClassAdresse='error';
		
	  }
	else
	  { //l'adresse est bien positionnee
		 if (preg_match("/([0-9]{2})*(\s)*([a-zA-Z]*(\s)*)*/", $_POST['adresse']) ) {
			 
         		$_SESSION[$_SESSION['usr']['name']]['adresse']=$_POST['adresse'];
			 
     } else 
		{
        
			$ChampIncorrectAdresse='C\'est une adresse incorrecte';
	        $ClassAdresse='error';
	   
}
		  
	  }

	//adresse electronique
	if(  (!isset($_POST['email'])) 			// la variable email n'est pas positionnée
	   ||(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))   
	  )
	  { 
		  $ChampIncorrectEmail='La variable email n\'est pas positionnee ou pas bien formé';
	      $ClassEmail='error';
	    
	  }
	else{
		
		  $_SESSION[$_SESSION['usr']['name']]['email']=$_POST['email'];
		
		}
	//ville
	if(!isset($_POST['ville']) || $_POST['ville']==''){
		 
		$ChampIncorrectVille='La ville n\'est pas positionnée';
		$ClassVille='error';
	}
    else{
		
		$_SESSION[$_SESSION['usr']['name']]['ville']=$_POST['ville'];
		
	}
	//code postal 
	if(  (!isset($_POST['postal'])))			// la variable postal n'est pas positionnée
	   
	  { 
		 
		  $ChampIncorrectPostal='La variable n\'est pas positionnée';
	      $ClassPostal='error';
	    
	  }
	else{//la variable postal est positionnee,et tester le code postal est bien 5 chiffres
		if (preg_match("/^[0-9]{5}$/", $_POST['postal']) ) {
			
         $_SESSION[$_SESSION['usr']['name']]['postal']=$_POST['postal'];
			
     } else 
		{
          
			$ChampIncorrectPostal='Code de postal est incorrect';
	        $ClassPostal='error';
	    
}
}
	
	//telephone
	if(  (!isset($_POST['tele'])))			// la variable postal n'est pas positionnée
	   
		  { 
			  $ChampIncorrectTele='La variable n\'est pas positionnée';
	          $ClassTele='error';
		    
	  }
	else{//la variable postal est positionnee,et tester le code postal est bien 5 chiffres
		if (preg_match("/^(0|\+33)[1-9]{1}[0-9]{8}/", $_POST['tele'])) {
            $_SESSION[$_SESSION['usr']['name']]['tele']=$_POST['tele'];
			
         } else 
		{
            $ChampIncorrectTele='Numéro de tele n\'est pas correct';
	        $ClassTele='error';
	   
        }
        }
	
	
	// Vérification de la date de naissance
    if(  (!isset($_POST['naissance'])) 		// la variable naissance n'est pas positionnée
       ||(trim($_POST['naissance'])=='')    // naissance vide
	  )
	  { 
		  
		  $ChampIncorrectNaissance='vous avez pas choisi la date';
	      $ClassNaissance='error';
		  
	  }
	else
	  { 
		   $_SESSION[$_SESSION['usr']['name']]['naissance']=$_POST['naissance'];
		   $systime=date('Y-m-d');
		  list($Annee,$Mois,$Jour)=explode('-',$_POST['naissance']);
		if(!checkdate($Mois,$Jour,$Annee)) 
		  {  
			
			  $ChampIncorrectNaissance='vous avez pas choisi la date';
			  $ClassNaissance='error';
			  
		  }
		  	else if(strtotime($_POST['naissance'])>strtotime($systime)){
              $ChampIncorrectNaissance='vous avez choisir la date incorrecte';
			  $ClassNaissance='error';
			  
 }
	  }
	
	
	
	
	
	
	
	
	
	
	//username ,password,repass
//la username est positionnee
	
	if (!isset($_POST['password']) || $_POST['password']==''){//la username est positionnee,mais passpord n'est pas positionnee
		
	$ChampIncorrectPassword='Mot de pass n\'est pas positionée';
	$ClassPassword='error';	
	
		
	}else{//la username est positionnee,passpord est positionnee
		
	if (!isset($_POST['repass']) || $_POST['repass']==''){//le username est positionnee,passpord est positionnee,mais
		                          //le repass n'est pas positionnee
		
			
	$ChampIncorrectRepass='Repass n\'est  pas positionée';
	$ClassRepass='error';	
	
		
	}else{//la username est positionnee,passpord est positionnee,
		  //la repass est  positionnee,mais le passport != repass
		
		
		$passport= $_POST['password'];
		$repass=$_POST['repass'];
		
		
		
		if ($ClassUsername=='ok'&&$ClassNom=='ok'&&$ClassPrenom=='ok'&&$ClassPassword=='ok'&&$ClassRepass=='ok'&&$ClassRepass=='ok'
	
	&&$ClassSexe=='ok'
    &&$ClassAdresse=='ok'
	&&$ClassEmail=='ok'
	&&$ClassVille='ok'
	&&$ClassPostal=='ok'
	&&$ClassTele=='ok'
	&&$ClassNaissance=='ok') {
		
		
		
		if (strcmp($passport,$repass )==0) {//passport =repass
                     //equivalent
             if (!isset( $_SESSION[$_SESSION['usr']['name']]['password'])|| $_SESSION[$_SESSION['usr']['name']]['password']!=''){//username n'est pas inscrire
			
			    $_SESSION[$_SESSION['usr']['name']]['password']=$_POST[password];
				 
				
				
			
			 }
           
		   }else{//password !=repass
                
			$ChampIncorrectPassword='';
	        $ChampIncorrectRepass='Password et repass sont pas de même';
			$ClassPassword='error';
			$ClassRepass='error';
	             	
				 }
	}
	}
	}
 

	

	if ($ClassUsername=='ok'&&$ClassNom=='ok'&&$ClassPrenom=='ok'&&$ClassPassword=='ok'&&$ClassRepass=='ok'&&$ClassRepass=='ok'
	
	&&$ClassSexe=='ok'
    &&$ClassAdresse=='ok'
	&&$ClassEmail=='ok'
	&&$ClassVille='ok'
	&&$ClassPostal=='ok'
	&&$ClassTele=='ok'
	&&$ClassNaissance=='ok') 
				//$toutvabien = ok
				
		{
		//si vous avez une formule bien forme ,qui va aller a la page de home .
		
		
		if (isset($_SESSION['usr'])){	
        	
			$_SESSION['usr']['isLogin'] = false;
		}
		else{
			
			$_SESSION['usr']=array();
			
			$_SESSION['usr']['isLogin'] = false;
		}
			header('Location:login.php');
			
			
			
		
	}


}

?>









<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" conten="text/html" charset="utf-8">
    <title>modifier <?php echo $_SESSION['usr']['name']; ?></title>
		
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="http://cdn.bootcss.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<link href="http://cdn.bootcss.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/htmleaf-demo.css">
	<link rel="stylesheet" type="text/css" href="css/bootsnav.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<style type="text/css">
		body
		{
			margin: 0;
			background-color: #FEDCD2;
		}
		.ok{
		   }
		.error
		{
		
			border-color: red;
			
		}
		.border{
			border-radius: 10px;
		}
		.incorrect{
			color: red;
		}
	</style>
</head>
	
	<body>
	<div class="demo" style="padding: 2em 0;">
	<div class="container">
	<?php Navigateur($Hierarchie); ?>
	<div class="row clearfix">
		<div class="col-md-12 column">
			<div class="row clearfix">
				<div class="col-md-2 column">
				</div>
				<div class="col-md-8 column">
				
						<form method="post" action="#" style="color:black; border-style:solid; padding-top:10%; padding-bottom:10%; border-color: #FF8765; text-align: center; border-radius: 10px;">
<fieldset>
    <legend><h2>Modifier des informations</h2></legend>
	
	 
	<br />		
	<br />	
	
	 Neauveau mots de passe : 
	<input  name="password" class="<?php echo $ClassPassword; ?> border"  value="<?php if(isset($_SESSION[$_SESSION['usr']['name']]['password']))  echo $_SESSION[$_SESSION['usr']['name']]['password']; ?>"
	 />
	<?php if (isset($ChampIncorrectPassword))echo '<br/><div class="incorrect">'.$ChampIncorrectPassword.'</div>';?> 
	<br />
    <br />	
    Verifier neauveau mots de passe : 
    <input  name="repass" class="<?php echo $ClassRepass; ?> border"
     value="<?php if(isset($_SESSION[$_SESSION['usr']['name']]['password']))  echo $_SESSION[$_SESSION['usr']['name']]['password']; ?>"
     />
     <?php if (isset($ChampIncorrectRepass)) echo  '<br/><div class="incorrect">'.$ChampIncorrectRepass.'</div>';?>
	
	<br/>
	<br/>	
	Sexe : 
 
  <span class="<?php echo $ClassSexe; ?>">
	<input type="radio" name="sexe" value="f" 
	<?php if((isset($_SESSION[$_SESSION['usr']['name']]['sexe']))&&($_SESSION[$_SESSION['usr']['name']]['sexe'])=='f') echo 'checked="checked"'; ?>
	/> Femme 	
	<input type="radio" class="<?php echo $ClassSexe; ?>" name="sexe" value="h"
	<?php if((isset($_SESSION[$_SESSION['usr']['name']]['sexe']))&&($_SESSION[$_SESSION['usr']['name']]['sexe'])=='h') echo 'checked="checked"'; ?>
	/> Homme
  </span>
  <?php if (isset($ChampIncorrectSexe))echo '<br/><div class="incorrect">'.$ChampIncorrectSexe.'</div>';?> 	
	<br />
    <br />	
    Nom :    
	<input type="text" class="<?php echo $ClassNom; ?> border" name="nom" required="required" 
    value="<?php if(isset($_SESSION[$_SESSION['usr']['name']]['nom']))  echo $_SESSION[$_SESSION['usr']['name']]['nom']; ?>"/>
    <?php if (isset($ChampIncorrectNom)) echo '<br/><div class="incorrect">'.$ChampIncorrectNom.'</div>';?> 
	<br />  
    <br />	 
    Prénom : 
	<input type="text" class="<?php echo $ClassPrenom; ?> border" name="prenom" 
	       value="<?php if(isset($_SESSION[$_SESSION['usr']['name']]['prenom']))  echo $_SESSION[$_SESSION['usr']['name']]['prenom']; ?>"/>
	<?php if (isset($ChampIncorrectPrenom)) echo '<br/><div class="incorrect">'.$ChampIncorrectPrenom.'</div>'; ?> 
    <br /> 
    <br />		
    Date de naissance : 
	<input type="date" class="<?php echo $ClassNaissance; ?> border" name="naissance" 
	       value="<?php if(isset($_SESSION[$_SESSION['usr']['name']]['naissance'])) echo $_SESSION[$_SESSION['usr']['name']]['naissance']; ?>"/>
    <?php if (isset($ChampIncorrectNaissance)) echo '<br/><div class="incorrect">'.$ChampIncorrectNaissance.'</div>'; ?> 
    <br /> 
	<br />	
	 
    Adresse électronique : 
	<input type="text" class="<?php echo $ClassEmail; ?> border" name="email" 
	       value="<?php if(isset($_SESSION[$_SESSION['usr']['name']]["email"]))  echo $_SESSION[$_SESSION['usr']['name']]["email"]; ?>"/>
	<?php if (isset($ChampIncorrectEmail))echo '<br/><div class="incorrect">'.$ChampIncorrectEmail.'</div>';?>
    <br /> 
    <br />		
    Code postal : 
	<input type="text" class="<?php echo $ClassPostal; ?> border" name="postal" 
	       value="<?php if(isset($_SESSION[$_SESSION['usr']['name']]['postal'])) echo $_SESSION[$_SESSION['usr']['name']]['postal']; ?>"/>
   <?php if (isset($ChampIncorrectPostal))echo '<br/><div class="incorrect">'.$ChampIncorrectPostal.'</div>';?>
    <br />
    <br />	
   Ville :
   <input type="text" class="<?php echo $ClassVille; ?> border" name="ville"
          value="<?php if(isset($_SESSION[$_SESSION['usr']['name']]['ville'])) echo $_SESSION[$_SESSION['usr']['name']]['ville']; ?>"/>
	<?php if (isset($ChampIncorrectVille))echo '<br/><div class="incorrect">'.$ChampIncorrectVille.'</div>';?>
    <br />
    <br />	
   Telephone :
   <input type="text" class="<?php echo $ClassTele; ?> border" name="tele"
          value="<?php if(isset($_SESSION[$_SESSION['usr']['name']]['tele'])) echo $_SESSION[$_SESSION['usr']['name']]['tele']; ?>"/>
	<?php if (isset($ChampIncorrectTele)) echo '<br/><div class="incorrect">'.$ChampIncorrectTele.'</div>';?>
	
	<br />
    <br />	
   Adresse :
   <input type="text" class="<?php echo $ClassAdresse; ?> border" name="adresse"
          value="<?php if(isset($_SESSION[$_SESSION['usr']['name']]['adresse'])) echo $_SESSION[$_SESSION['usr']['name']]['adresse']; ?>"/>
	<?php if (isset($ChampIncorrectAdresse)) echo '<br/><div class="incorrect">'.$ChampIncorrectAdresse.'</div>';?>
	
	</fieldset>
		<br />
		<input type="submit" name="submit" value="Confirm" style="background:#8FD8D2; border-radius: 5px;border-color: #fff; height: 30px; width: 100px;"/>
		</form>
		        </div>
				  <div class="col-md-2 column">
				</div>
			</div>
		</div>
	</div>
</div>
	</div>
	<script src="js/jquery-1.11.0.min.js" type="text/javascript"></script>
 
	<script src="http://cdn.bootcss.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/bootsnav.js"></script>
		</body>
	</html>
	








