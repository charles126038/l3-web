<!DOCTYPE html>
<?php include ("Donnees.inc.php");?>
<?php include ("Fonction/donneeTraiter.php");?>
<?php include ("Fonction/fonctionSearch.php");?>
<?php 
	session_start();
	if(isset($_GET['logOut'])){
		if($_GET['logOut'] == 1){
			if(isset($_SESSION['usr']))
				$_SESSION['usr']["isLogin"] = false;
		}
	}
	if(isset($_GET['addfavorite'])){
		if(isset($_COOKIE['favorite'])){
			$favorite = unserialize($_COOKIE['favorite']);
			ajouterFavorite($_GET['addfavorite'], $favorite);
			setcookie('favorite', serialize($favorite));
			$_COOKIE['favorite'] = serialize($favorite);
		}
		else{
			$favorite = array();
			ajouterFavorite($_GET['addfavorite'], $favorite);
			setcookie('favorite', serialize($favorite));
			$_COOKIE['favorite'] = serialize($favorite);
		}
	}
	$searchName;
	$ajouter = "";
	$supprime = "";
	if(isset($_GET['searchName']))
		$searchName = $_GET['searchName'];
	if(isset($_GET['ajouter']))
		$ajouter = $_GET['ajouter'];
	if(isset($_GET['supprime']))
		$supprime = $_GET['supprime'];
?>


<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Search</title>
	<link href="http://cdn.bootcss.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<link href="http://cdn.bootcss.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/htmleaf-demo.css">
	<link rel="stylesheet" type="text/css" href="css/bootsnav.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	

		<style>
		
*
{
    outline: none;
}

html, body
{
    height: 100%;
    min-height: 100%;
}

body
{
    margin: 0;
    background-color: #FEDCD2;
}







#wrap{
    display: flex;
	justify-content:space-around;
}


	
#association{
	color: #5D5D5D;
	border-radius:20px;
	text-align: center;
	box-shadow: 0 10px 20px -5px rgba(255, 117, 117, 0.86);
}
#association li {
    border: 1px solid #ddd; 
	border-radius:20px;
}
#association li:hover{
	color: #FFCACA;
	background-color: #ff7575;
}
			
.border{
	border-radius: 10px;
	font-size: 30px; 
	border: 1 black solid;
}
	</style>

<script>	
	function lookup(search) {
		document.getElementById("associationsList").innerHTML = search.length;
        if(search.length == 0) {
            $('#association').hide();
        } else {
            $.get("ajax/searchDonne.php", {'mot': ""+search+""}, function(data){
            	$('#association').show();
            	$('#associationsList').html(data);
            });
        }
    }

	
    function fill(thisValue) {
        $('#search').val(thisValue);
        setTimeout("$('#association').hide();", 200);
	}
</script>

</head>


<body>
    <div class="demo" style="padding: 2em 0;">
        <div class="container">
            <?php Navigateur($Hierarchie) ?>
            <div class="row clearfix">   
           		<div class="col-md-12 column">
           		
           			<div class="col-md-4" >
				    	<form method="get" action="?" name="ajouter"style="color:black;  margin-top:-10%; padding-bottom:10%;  text-align: center">
					    		
						<p>Donner les ingredients que vous voulez en form ci-dessous </p>
					  	 
						   <input type="text" name="ajouter" class = "border" placeholder="sel;citron" value="<?php echo $ajouter; ?>" />
						  
					    	 <br/><br/>
							
							 <input type="submit" name="submit" value="ajouter" style="background:#8FD8D2; border-radius: 5px;border-color: #fff; height: 30px; width: 100px;"/>
							
						  
					
					
							<input type="hidden" name="searchName" value="<?php echo $searchName; ?>" />
							<input type="hidden" name="supprime" value="<?php echo $supprime; ?>" />
						
					  </form>
					  
					  
					  <?php
							
							if(isset($_GET["ajouter"])){
								$a = $_GET['ajouter'];
								$ajou = explode(";",$a);
								echo '<div id = "wrap">';
									foreach($ajou as $indice => $var){
										echo '<div>';
										echo '<span class="label label-default">';
										echo $var;
										echo '</span>';
										echo '</div>';
										
									}
								echo '</div>';
							}
							
							?>
				    </div>
          				
						
          
          			<div class="col-md-4"  >
					  <form method="get" action="?"name = "search" class = "border"style="color:black;  margin-top:25px; padding-bottom:10%;  text-align: center">
					    		
					   
						
					  	 
						 <input type="text" name="searchName" placeholder="Search" required="required" onkeyup="lookup(this.value)"  value="<?php echo $searchName?>"  class = "border">
						  
						   <br/>
					  	<div style="padding-top: 15px">
					 		<input type="submit" name="submit" value="search" style="background:#8FD8D2; border-radius: 5px;border-color: #fff; height: 30px; width: 100px; font-size: 17px "/>
						
						</div>
						
						<ul class="search-layer list-unstyled" id="association">
							<span id="associationsList"> </span>
						</ul>
					  </form>
					</div>
          
          
          
          
          			 <div class="col-md-4">
			    			<form method="get" action="?" name="supprime"style="color:black;  margin-top:-10%; padding-bottom:10%;  text-align: center">
					    		
						<p>Donner les ingredients que vous ne voulez pas en form ci-dessous </p>
					  	 
						   <input type="text" name="supprime" class = "border" placeholder="sel;citron" value="<?php echo $supprime; ?>"/>
						  
					    	 <br/><br/>
							
							 <input type="submit" name="submit" value="supprime" style="background:#8FD8D2; border-radius: 5px;border-color: #fff; height: 30px; width: 100px"/>
							
						    <input type="hidden" name="searchName" value="<?php echo $searchName; ?>" />
							<input type="hidden" name="ajouter" value="<?php echo $ajouter; ?>" />
						
						
						
					  </form>
					  
					  <?php
							
							if(isset($_GET["supprime"])){
								$b = $_GET['supprime'];
								$sup = explode(";",$b);
								echo '<div id = "wrap">';
									foreach($sup as $indice => $var){
										echo '<div>';
										echo '<span class="label label-default">';
										echo $var;
										echo '</span>';
										echo '</div>';
										
									}
								echo '</div>';
							}
							
							?>
					  </div>
           
           
           		</div>
			</div>
        
        	
         
           <div class="row clearfix" style="z-index: -1">
           <div class="col-md-12 column">
           	
            	<?php 
			    
			   $list = listCherche($_GET['searchName'], $Recettes, $Hierarchie);
				modifier($ajouter, $supprime, $list, $Recettes);
			afficherListeCherche($_GET['searchName'], $list, $ajouter, $supprime); ?>
			
			</div>
			</div>
        </div>
        </div>
    

    <script src="js/jquery-1.11.0.min.js" type="text/javascript"></script>
 
	<script src="http://cdn.bootcss.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/bootsnav.js"></script>
</body>
</html>