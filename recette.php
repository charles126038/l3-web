<!DOCTYPE html>
<?php include ("Donnees.inc.php");?>
<?php include ("Fonction/donneeTraiter.php");?>
<?php 
	session_start();
	if(isset($_GET['addfavorite'])){
		if(isset($_COOKIE['favorite'])){
			$favorite = unserialize($_COOKIE['favorite']);
			ajouterFavorite($_GET['addfavorite'], $favorite);
			setcookie('favorite', serialize($favorite));
			$_COOKIE['favorite'] = serialize($favorite);
		}
		else{
			$favorite = array();
			ajouterFavorite($_GET['addfavorite'], $favorite);
			setcookie('favorite', serialize($favorite));
			$_COOKIE['favorite'] = serialize($favorite);
		}
	}
	if(isset($_GET['recette']))
		$recette = $_GET['recette'];
	else
		$recette = "Bloody Mary";



	$tabConversion = array(	
				'Á' => 'a', 'Â' => 'a', 'Ä' => 'a', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ä' => 'a', '@' => 'a',
				'È' => 'e', 'É' => 'e', 'Ê' => 'e', 'Ë' => 'e', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 
				'Ì' => 'i', 'Í' => 'i', 'Î' => 'i', 'Ï' => 'i', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
				'Ò' => 'o', 'Ó' => 'o', 'Ô' => 'o', 'Ö' => 'o', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'ö' => 'o',
				'Ù' => 'u', 'Ú' => 'u', 'Û' => 'u', 'Ü' => 'u', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u',
				'Œ' => 'oe', 'œ' => 'oe',
				'ç' => 'c',
				' ' => '_'
		);
?>

<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $recette; ?></title>
	<link href="http://cdn.bootcss.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<link href="http://cdn.bootcss.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/htmleaf-demo.css">
	<link rel="stylesheet" type="text/css" href="css/bootsnav.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<style>
	body
	{
		margin: 0;
		background-color: #FEDCD2;
	}
	</style>
</head>

<body>
   
    <div class="demo" style="padding: 2em 0;">
       
 
        <div class="container">
       
          <?php Navigateur($Hierarchie) ?>
         
				 <?php
				$titre = $_GET["recette"];
				foreach($Recettes as $indice => $var){
					if(strcmp($var['titre'] , $titre) == 0){
						$ingredients = $var['ingredients'];
						$preparation = $var['preparation'];
						$index = $var['index'];
						$tabingredients = preg_split('/[|]/', $ingredients);
						$tabpreparation =preg_split('/[.]/' , $preparation);
						
					}
				}
			?>
				
				<div class="row">       
				  <div class="col-md-12 column">             
				  		<div class="page-header">                
				  			 <h1 style="color:#DF744A; text-shadow: 6px 6px 3px #FF9F84;text-align: center">                    
				  			 					<?php echo $titre;?>         
				  			</h1>             
				  		</div>         
				  	</div>     
				 </div>
			
       		<div class="row clearfix">
       		  <div class="col-md-3 column"  style="text-align: center">
       		  
       		  <?php
				  $premierLettre = $recette[0];
				$r = strtolower($recette);
				$r[0] = $premierLettre;
				$r = strtr($r, $tabConversion);
				
				$photo = $r.".jpg";
			if(file_exists("Photos/".$photo))
				echo '<img alt="140x140" src="Photos/'.$photo.'" style="max-height:300px; max-width:250px; border-radius:50px; box-shadow: 5px 5px 5px #FFB19A;"/>';?>
		</div>
		
		
		<div class="col-md-6 column" >
			<div class="panel-group" id="panel-934177">
				<div class="panel panel-default">
					<div class="panel-heading"style="text-align: center;color:#df744a;background-color:#8fd8d2;font-weight: bold">
						 <a class="panel-title" data-toggle="collapse" data-parent="#panel-934177" href="#panel-element-643421">Ingredients</a>
					</div>
					<div id="panel-element-643421" class="panel-collapse in">
						<div class="panel-body"style="color: black;text-align: center">
							<?php foreach($tabingredients as $indice=>$var){
									echo $var;
									echo '<br/>';
	
	
							}?>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading" style="text-align: center;color:#df744a;background-color:#8fd8d2;font-weight: bold">
						 <a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-934177" href="#panel-element-278144">Preparation</a>
					</div>
					<div id="panel-element-278144" class="panel-collapse in">
						<div class="panel-body" style="color: black">
							<ul>
							<?php for($i = 0 ; $i < sizeof($tabpreparation)-1;$i++){
									echo '<li>'.$tabpreparation[$i];
									echo '<br/><br/></li>';
	
	
							}?>
							</ul>
						</div>
					</div>
				</div>
					<div class="panel panel-default">
					<div class="panel-heading" style="text-align: center;color:#df744a;background-color:#8fd8d2;font-weight: bold">
						 <a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-934177" href="#panel-element-278145">Aliments</a>
					</div>
					<div id="panel-element-278145" class="panel-collapse in">
						<div class="panel-body" style="color: black;text-align: center">
							<?php foreach($index as $indice=>$var){
									echo $var;
									echo '<br/>';
	
	
							}?>
						</div>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-md-9 column">
				</div>
				<div class="col-md-3 column">
					<a href="recette.php?recette=<?php echo $recette ?>&addfavorite=<?php echo $recette ?>"><button type="button" class="btn btn-info" style="max-height:50px; ">Ajouter à favorite</button></a>
				</div>
			</div>
		
		</div>
		  <div class="col-md-3 column" style="text-align: center">
		   <?php
				  $premierLettre = $recette[0];
				$r = strtolower($recette);
				$r[0] = $premierLettre;
				$r = strtr($r, $tabConversion);
				
				$photo = $r.".jpg";
			if(file_exists("Photos/".$photo))
				echo '<img alt="140x140" src="Photos/'.$photo.'" style="max-height:300px; max-width:250px; border-radius:50px; box-shadow: 5px 5px 5px #FFB19A;"/>';?>
		</div>
	</div>		
		
		
	</div>
          
           
        </div>
   
   
 
	
    <script src="js/jquery-1.11.0.min.js" type="text/javascript"></script>
	<script src="http://cdn.bootcss.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/bootsnav.js"></script>
</body>
</html>