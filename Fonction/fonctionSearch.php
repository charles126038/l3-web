<?php

function searchDonne($mot, $recettes){
	$list = array();
	if(trim($mot) == "")
		return array();
	foreach($recettes as $indice => $content){
		foreach($content as $catego => $array){
			if(!strcmp($catego, 'titre')){
				if(preg_match("/".$mot."/i", $array)){
					array_push($list, '<li class="search-layer-item text-ellipsis" onClick="fill(\''.$array.'\');"><h2>'.$array.'</h2></li>');
					if(count($list) != count(array_unique($list))){
						array_splice($list, sizeof($list)-1, 1);
					}
				}
			}
			if(!strcmp($catego, 'index')){
				foreach($array as $index => $aliment){
					if(preg_match("/".$mot."/i", $aliment)){
						array_push($list, '<li class="search-layer-item text-ellipsis" onClick="fill(\''.$aliment.'\');"><h2>'.$aliment.'</h2></li>');
						if(count($list) != count(array_unique($list))){
							array_splice($list, sizeof($list)-1, 1);
						}
					}
				}
			}
		}
	}
	return $list;
}

function listCherche($mot, $recettes, $hierar){
	$list = array();
	if(trim($mot) == "")
		return array();
	foreach($recettes as $indice => $content){  // construire une liste des recettes selon Recettes
		foreach($content as $catego => $array){
			if(!strcmp($catego, 'titre')){
				if(preg_match("/^[".$mot."]$/i", $array)){
					echo $array;
					$res = array('recette' => $array, 'prior' => 100);
					array_push($list, $res);
				}
				else if(preg_match("/".$mot."/i", $array)){
					$res = array('recette' => $array, 'prior' => 20);
					array_push($list, $res);
				}
				else{
					$search = array();
					$search = explode(' ', $mot);
					$prior = 0;
					if(sizeof($search) != 0){
						foreach($search as $index => $m){
							if(preg_match("/".$m."/i", $array)){
								$prior = $prior + 20;
							}
						}
						$res = array('recette' => $array, 'prior' => $prior);
						array_push($list, $res);
					}
				}
				
			}
			if(!strcmp($catego, 'index')){
				foreach($array as $index => $aliment){
					if(preg_match("/".$mot."/i", $aliment)){
						$res = array('recette' => $content['titre'], 'prior' => 20);
						array_push($list, $res);
					}
				}
			}
		}
	}
	
	// les recettes selon les aliments hierarchie
	$tab = getPremier($hierar);
	$listHierar = construitListParHierar($tab, $hierar);  // la liste de hierarchie
	$tab = listeAlimentHierarchie($mot, $listHierar);
	$l = listeAlimentSelonSearch($tab);
	
	$res = array();
	for($i = 0; $i < sizeof($l); $i++){
		foreach($recettes as $indice => $elements){
			foreach($elements as $element => $information){
				if(!strcmp($element, "index")){
					foreach($information as $ali){
						if(!strcmp($l[$i], $ali)){
							$iter = array('recette' => $elements['titre'], 'prior' => 20);
							array_push($res, $iter);
						}
					}
				}
			}
		}
	}
	$list = array_merge($list, $res);
	classerList($list);
	$tmp_arr = array();
    foreach ($list as $k => $v) {
        if (in_array($v['recette'], $tmp_arr)) {
            unset($list[$k]);
        } else {
            $tmp_arr[] = $v['recette'];
        }
    }
    sort($list);
	return $list; // retourner la liste des recettes avec satisfaction
}

function getPremier($tabHierarchie){  // return les aliments qui sont juste des fils du categorie "Aliment"
		$hierar = $tabHierarchie;  // tabHierarchie tableau source (donnees.inc.php)
		$tab = array();
		foreach($hierar as $aliment => $indice){ // permier categories
			foreach($indice as $categories => $categorie){
				if(!strcmp($categories, "super-categorie")){
					if(!strcmp(current($categorie), "Aliment")){
						array_push($tab, $aliment);
					}
				}
			}
		}
		
		return $tab;

}

function construitListParHierar($tab, $tabHierarchie){  // return un tableau trie par navigateur (fruit-agrume-orange..)
		$hierar = $tabHierarchie;  // tabHierarchie tableau source (donnees.inc.php)
		$navigateur = array();
		foreach($tab as $nom){  // tab un tableau avec les noms d'aliment (array([0]=>fruit, [1]=>oeuf..))
			foreach($hierar as $aliment => $indice){
				foreach($indice as $categories => $categorie){
					if(!strcmp($aliment, $nom)){   // chercher les aliments avec des noms qui sont de meme dans tabHierar
						if(!strcmp($categories, "sous-categorie")){  // mettre les fils aliments par recursif
							$navigateur[$nom] = construitListParHierar($categorie, $tabHierarchie);
						}
						if(sizeof($indice) == 1) // si il existe plus fils (feuille)
							array_push($navigateur, $aliment);
					}
				}
			}
		}
		
		return($navigateur);

}


/**  retourner la liste qui contient l'aliment qu'utilisateur a choisi et son hierarchie
*/
function listeAlimentHierarchie($mot, $hierar){
	$list = array();
	foreach($hierar as $indice=>$var){
		if(preg_match("/[0-9]/" , $indice)){
			if(preg_match("/".$mot."/i", $var)){
				array_push($list, $var);
			}
		}
		else{
			if(preg_match("/".$mot."/i", $indice)){
				$list = array_merge($list, $var);
			}
			else{
				$res = array();
				$res = listeAlimentHierarchie($mot, $var);
				$list = array_merge($list, $res);
			}
				
		}
	}
	return $list;
}

// retourner les aliments hierarchie utilisant @$listHierar -- listeAlimentHierarchie
function listeAlimentSelonSearch($listHierar){
	$list = array();
	foreach($listHierar as $indice => $array){
		if(preg_match("/[0-9]/" , $indice)){
			array_push($list, $array);
		}
		else{
			listeAlimentSelonSearch($array);
		}
	}
	$list = array_unique($list);
	$list = array_values($list);
	return $list;
}


function classerList(&$list){
	for($i = 0; $i < sizeof($list) - 1; $i++){
		for($j = 0; $j < sizeof($list) - 1 - $i; $j++){
			if ($list[$j]['prior'] < $list[$j+1]['prior']) {
				$tmp = $list[$j];
				$list[$j] = $list[$j+1];
				$list[$j+1] = $tmp;
			}
		}
	}
	foreach($list as $index => $array){
		if($list[$index]['prior'] == 0) unset($list[$index]);
	}
}


function afficherListeCherche($searchName, $list, $add, $sup){
	classerList($list);
	$i = 0;
	if(sizeof($list) > 0) {
		while($i < sizeof($list)){
			if(sizeof($list)-$i <= 1){	
				echo '
				<div class="nav navbar-nav">
				<div class="col-md-4 column">
					';if($i < sizeof($list)){ afficherImageCherche($searchName, $list[$i]['recette'], $add, $sup); $i++; }echo '
				</div>
				<div class="col-md-4 column">
				</div>
				<div class="col-md-4 column">
				</div>
				</div>';
			}
			else if(sizeof($list)-$i == 2){	
				echo '
				<div class="nav navbar-nav">
				<div class="col-md-4 column">
					';if($i < sizeof($list)){ afficherImageCherche($searchName,$list[$i]['recette'], $add, $sup); $i++; }echo '
				</div>
				<div class="col-md-4 column">
					';if($i < sizeof($list)){ afficherImageCherche($searchName,$list[$i]['recette'], $add, $sup); $i++; }echo '
				</div>
				<div class="col-md-4 column">
				</div>
				</div>';
			}
			else{
				echo '
				<div class="nav navbar-nav" >
				<div class="col-md-4 column">
					';if($i < sizeof($list)){ afficherImageCherche($searchName,$list[$i]['recette'], $add, $sup); $i++; }echo '
				</div>
				<div class="col-md-4 column">
					';if($i < sizeof($list)){ afficherImageCherche($searchName,$list[$i]['recette'], $add, $sup); $i++; }echo '
				</div>
				<div class="col-md-4 column">
					'; if($i < sizeof($list)){ afficherImageCherche($searchName,$list[$i]['recette'], $add, $sup); $i++; }echo '
				</div>
				</div>';
			}
		}
	}
}


/** Afficher les images et titre des recettes 
@searchName - nom qu'utilisateur est en train de chercher
@recette - recette a afficher
*/
function afficherImageCherche($searchName, $recette, $add, $sup){
	
	//un tableau de remplacement
	$tabConversion = array(	
				'Á' => 'a', 'Â' => 'a', 'Ä' => 'a', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ä' => 'a', '@' => 'a',
				'È' => 'e', 'É' => 'e', 'Ê' => 'e', 'Ë' => 'e', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 
				'Ì' => 'i', 'Í' => 'i', 'Î' => 'i', 'Ï' => 'i', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
				'Ò' => 'o', 'Ó' => 'o', 'Ô' => 'o', 'Ö' => 'o', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'ö' => 'o',
				'Ù' => 'u', 'Ú' => 'u', 'Û' => 'u', 'Ü' => 'u', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u',
				'Œ' => 'oe', 'œ' => 'oe',
				'ç' => 'c',
				' ' => '_'
		);
	echo '
			<div class="thumbnail" style="min-height:450px; min-width:320px; max-height:510px; max-width:320px; resize:horizontal; margin-left:50px; background:#FEDCD2; border-radius:50px; border-color:#FEDCD2; box-shadow: 10px 5px 5px #FFB19A;padding-top:5%;">';
			$premierLettre = $recette[0];
			$r = strtolower($recette);
			$r[0] = $premierLettre;
			$r = strtr($r, $tabConversion);
			$photo = $r.".jpg";
			if(file_exists("Photos/".$photo))
				echo '<img alt="140x140" src="Photos/'.$photo.'" style="max-height:300px; max-width:250px; border-radius:50px; box-shadow: 5px 5px 5px #FFB19A;"/>';
		echo  '	<div class="caption">
					<h3>
						<a class="btn" href="recette.php?recette='.$recette.'" style="color: #3d4444" onmouseover="this.style.color='."'#df6659';".'" onmouseout="this.style.color='."'#3d4444';".'"'.'>'.$recette.'</a>
					</h3>';
			echo '<a href="?searchName='.$searchName.'&addfavorite='.$recette.'&ajouter='.$add.'&supprime='.$sup.'"><button type="button" class="btn btn-info" style="margin-left:140px; max-height:30px; ">Ajouter à favorite</button></a>';
					
			echo '</div>
				
				</div>';
}

// modifier la liste des recettes correspondent a les ingredients qu'utilisateur veut et ne veut pas
function modifier($ingredientAdd, $ingredientSup, &$list, $Recette){
	foreach($list as $index => $array){
		foreach($Recette as $indince => $arr){
			foreach($arr as $titre => $content){
				if(!strcmp("titre", $titre)){ 
					if(!strcmp($array['recette'], $content)){ // si trouver la recette
						foreach($arr['index'] as $n => $ingredient){  // cherche les ingredients contient ou pas les "adds" ou "supprime"
							if(strcmp($ingredientAdd, "")){
								$add = explode(";", $ingredientAdd);
								foreach($add as $i => $a){
									if(preg_match("/".$a."/i", $ingredient))
										$list[$index]['prior'] = $list[$index]['prior'] + 2;
								}
							}
							
							if(strcmp($ingredientSup, "")){
								$sup = explode(";", $ingredientSup);
								$boolSup = false;

								foreach($sup as $i => $s){
									if(preg_match("/".$s."/i", $ingredient)){
										$list[$index]['prior'] = 0;
										$boolSup = true;
									}
								}
								if($boolSup == true)
									continue;
							}
							
						}
					}
				}
			}
		}
	}
	classerList($list);
	return $list;
}

?>