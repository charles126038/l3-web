<?php


//---------------------------------------------------------------------------------------------------------------------//

//--------------------------------------------------- fonction privee -------------------------------------------------//

//---------------------------------------------------------------------------------------------------------------------//

	function getAlimentSuper($tabHierarchie){  // return les aliments qui sont juste des fils du categorie "Aliment"
		$hierar = $tabHierarchie;  // tabHierarchie tableau source (donnees.inc.php)
		$tab = array();
		array_push($tab, "chemin");
		foreach($hierar as $aliment => $indice){ // permier categories
			foreach($indice as $categories => $categorie){
				if(!strcmp($categories, "super-categorie")){
					if(!strcmp(current($categorie), "Aliment")){
						array_push($tab, $aliment);
					}
				}
			}
		}
		
		return $tab;
	}

	function construitNavigateur($tab, $tabHierarchie, $chemin, $pere){  // return un tableau trie par navigateur (fruit-agrume-orange..)
		$hierar = $tabHierarchie;  // tabHierarchie tableau source (donnees.inc.php)
		$navigateur = array();
		foreach($tab as $nom){  // tab un tableau avec les noms d'aliment (array([0]=>fruit, [1]=>oeuf..))
			foreach($hierar as $aliment => $indice){
				foreach($indice as $categories => $categorie){
					if(!strcmp("chemin", $nom)){
						if(!isset($navigateur['chemin']))
							$navigateur['chemin'] = $chemin;
					}
					if(!strcmp($aliment, $nom)){   // chercher les aliments avec des noms qui sont de meme dans tabHierar
						if(!strcmp($categories, "sous-categorie")){  // mettre les fils aliments par recursif
							if(!isset($navigateur['chemin'])){
								$chemin = $chemin."/".$pere;
								$navigateur['chemin'] = $chemin;
							}
							$pere = $aliment;
							$navigateur[$nom] = construitNavigateur($categorie, $tabHierarchie, $chemin, $pere);
							
							
						}
						if(sizeof($indice) == 1){ // si il existe plus fils (feuille)
							if(!isset($navigateur['chemin'])){
								$chemin = $chemin."/".$pere;
								$navigateur['chemin'] = $chemin;
							}
							array_push($navigateur, $aliment);
						}
					}
				}
			}
		}
		
		return($navigateur);
	}

	function ListeNavigateur($tabHierarchie){ // construit le tableau trie par navigateur avec les deux fonctions privees
		$tab = getAlimentSuper($tabHierarchie);
		return construitNavigateur($tab, $tabHierarchie, "Aliment", "Aliment");
	}


/** Construire le navigateur de la liste d'aliment */
function AfficheNavigateur($tab){
	foreach($tab as $indice=>$var){
		if(preg_match("/[0-9]/" , $indice)){
			echo '<li>';
			echo '<a href ="aliment.php?aliment='.$var.'">'.$var.'</a>'; // remplacer ' ' a '_'
			echo '</li>';
		}
		else if(strcmp($indice, "chemin") != 0){
			echo '<li class="dropdown">';
			echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown">';
			echo $indice.'</a>';
			echo ' <ul class="dropdown-menu " style="background:#8FD8D2; border-radius:10px;">';
			AfficheNavigateur($var);
			echo '</ul>';
			echo '</li>';
		}
	}
}

/** Chercher les recettes qui contient de ingredient qu'utilisateur choisi
*@aliment - ingredient
*@listeRecettes - la liste des recettes dans donnees
*/
function chercheRecettes($aliment, $listeRecettes){
	$recettes = array();
	
	foreach($listeRecettes as $indice => $elements){
		foreach($elements as $element => $information){
			if(!strcmp($element, "index")){
				foreach($information as $ali){
					if(!strcmp($aliment, $ali)){
						array_push($recettes, $elements['titre']);
					}
				}
			}
		}
	}
	return $recettes;
}


//---------------------------------------------------------------------------------------------------------------------//

//------------------------------------------------- fonction public ------------------------------------------------//

//---------------------------------------------------------------------------------------------------------------------//

	

	/** ajouter une recette dans la liste de favorite
	 * @$recette - la recette ajoutee
	 * @$listFavorite - la liste des recettes favorite
	*/
	function ajouterFavorite($recette, &$listFavorite){
		$bool = true;
		if(isset($_SESSION['usr'])){
			if($_SESSION['usr']['isLogin'] == true){ // si deja login
				$nom = $_SESSION['usr']['name'];
				if(!isset($_SESSION[$nom]['favorite'])){
					$_SESSION[$nom]['favorite'] = array();
				}
				else{
					foreach($_SESSION[$nom]['favorite'] as $r){
						if(!strcmp($r, $recette)){ // si la liste contient une meme recette que la recette ajoutee
							$bool = false;
							break;
						}
					}
					if($bool == true)  // si la liste contient pas de recette ou pas de meme recette que la recette ajoutee
						array_push($_SESSION[$nom]['favorite'], $recette);
				}
			}
			else{  // si pas encore login
				$bool = true;
				if(sizeof($listFavorite) > 0){  // si la liste contient des recettes
					foreach($listFavorite as $r){
						if(!strcmp($r, $recette)){ // si la liste contient une meme recette que la recette ajoutee
							$bool = false;
							break;
						}
					}
				}
				if($bool == true)  // si la liste contient pas de recette ou pas de meme recette que la recette ajoutee
					array_push($listFavorite, $recette);
			}
		}else{  // si pas encore register 
			$bool = true;
				if(sizeof($listFavorite) > 0){  // si la liste contient des recettes
					foreach($listFavorite as $r){
						if(!strcmp($r, $recette)){ // si la liste contient une meme recette que la recette ajoutee
							$bool = false;
							break;
						}
					}
				}
				if($bool == true)  // si la liste contient pas de recette ou pas de meme recette que la recette ajoutee
					array_push($listFavorite, $recette);
		}
	}

	/** supprimer une recette dans la liste de favorite
	 * @$recette - la recette supprimee
	 * @$listFavorite - la liste des recettes favorite
	*/
	function deleteFavorite($recette, &$listFavorite){
		if(isset($_SESSION['usr'])){
			if($_SESSION['usr']['isLogin'] == true){ // si deja login
				$nom = $_SESSION['usr']['name'];
				for($i = 0; $i < sizeof($_SESSION[$nom]['favorite']); $i++){
					if(!strcmp($recette, $_SESSION[$nom]['favorite'][$i])){
						array_splice($_SESSION[$nom]['favorite'], $i, 1);
						break;
					}
				}
				$listFavorite = $_SESSION[$nom]['favorite'];
			}
			else{ // si pas encore login
				for($i = 0; $i < sizeof($listFavorite); $i++){
					if(!strcmp($recette, $listFavorite[$i])){
						array_splice($listFavorite, $i, 1);
						break;
					}
				}
			}
		}
		else{ // si pas encore inscrire
			for($i = 0; $i < sizeof($listFavorite); $i++){
				if(!strcmp($recette, $listFavorite[$i])){
					array_splice($listFavorite, $i, 1);
					break;
				}
			}
		}
	}




/** Construire le navigateur sur la site 
*@liste - $Hierarchie
*/
function Navigateur($liste){
	echo '<div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-default navbar-mobile bootsnav" style="background:#8FD8D2; border-radius:10px; padding-right:10px;">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="navbar-menu" bootsnav">
                            <ul class="nav navbar-nav" data-in="fadeInDown" data-out="fadeOutUp">
                                <li><a href="index.php">Accueil</a></li>
                                <li><a href="favorite.php">Favorite</a></li>
                                <li class="dropdown">
                                	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Aliment</a>
                                	<ul class="dropdown-menu" style="background:#8FD8D2; border-radius:10px;">';
											$tab = ListeNavigateur($liste);
											AfficheNavigateur($tab);
                               		echo '</ul>
                               	</li>
							</ul>
                            
                       		
							
							
							 ';

							if(isset($_SESSION["usr"])){
								if($_SESSION["usr"]['isLogin'] == true){
									$nom = $_SESSION["usr"]['name'];
								}
							}
							echo '<ul class="nav navbar-nav navbar-right">';
							if(isset($nom)){
								echo '<div style="margin-top:15px;">';
								echo '<a style="font-style:italic">Welcome, '.$nom.'</a><br/>';
								echo '<a href="modifier.php" style="margin-left:30px;text-decoration:underline;">Modifier</a>';
								echo '<a href="index.php?logOut=1" style="margin-left:10px;text-decoration:underline;">Log out</a></div>';
							}
							else{
								echo '
                                <li><a href="login.php">Login</a></li>
                                <li><a href="register1.php">register</a></li>';
							}echo '
							</ul>
                        </div>
                    </nav>
                </div>
            </div>';

}

/** Afficher les images et titre des recettes 
@aliment - titre de aliment
@recette - recette a afficher
@page - la page qu'utilisateur est sur
*/
function afficherImage($aliment, $recette, $page){
	
	//un tableau de remplacement
	$tabConversion = array(	
				'Á' => 'a', 'Â' => 'a', 'Ä' => 'a', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ä' => 'a', '@' => 'a',
				'È' => 'e', 'É' => 'e', 'Ê' => 'e', 'Ë' => 'e', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 
				'Ì' => 'i', 'Í' => 'i', 'Î' => 'i', 'Ï' => 'i', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
				'Ò' => 'o', 'Ó' => 'o', 'Ô' => 'o', 'Ö' => 'o', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'ö' => 'o',
				'Ù' => 'u', 'Ú' => 'u', 'Û' => 'u', 'Ü' => 'u', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u',
				'Œ' => 'oe', 'œ' => 'oe',
				'ç' => 'c',
				' ' => '_'
		);
	echo '
			<div class="thumbnail" style="min-height:450px; min-width:320px; max-height:510px; max-width:320px; resize:horizontal; margin-left:50px; background:#FEDCD2; border-radius:50px; border-color:#FEDCD2; box-shadow: 10px 5px 5px #FFB19A;padding-top:5%;">';
			$premierLettre = $recette[0];
			$r = strtolower($recette);
			$r[0] = $premierLettre;
			$r = strtr($r, $tabConversion);
			$a = strtr($aliment, $tabConversion);
			$photo = $r.".jpg";
			if(file_exists("Photos/".$photo))
				echo '<img alt="140x140" src="Photos/'.$photo.'" style="max-height:300px; max-width:250px; border-radius:50px; box-shadow: 5px 5px 5px #FFB19A;"/>';
		echo  '	<div class="caption">
					<h3>
						<a class="btn" href="recette.php?recette='.$recette.'" style="color: #3d4444" onmouseover="this.style.color='."'#df6659';".'" onmouseout="this.style.color='."'#3d4444';".'"'.'>'.$recette.'</a>
					</h3>';
		if(!strcmp($page, "aliment")){
				echo '<a href="?aliment='.$aliment.'&addfavorite='.$recette.'"><button type="button" class="btn btn-info" style="margin-left:140px; max-height:30px; ">Ajouter à favorite</button></a>';
		}
		if(!strcmp($page, "favorite")){
			echo '<a href="?delfavorite='.$recette.'"><button type="button" class="btn btn-danger" style="margin-left:200px; max-height:30px; margin-top:5px;">delete</button></a>';
		}
					
			echo '</div>
				
				</div>';
}

/** afficher le chemin de aliment vers le aliment precice
*@tab - la liste d'aliment triee
*@aliment - aliment qu'utilisateur choisi
*/
function afficherChemin($tab, $aliment){
	$chemin = "Aliment";
	if(!is_string($tab)){
	foreach($tab as $index => $array){
		
		if(strcmp($index, "chemin") != 0){
			if(preg_match("/[0-9]/" , $index)){
				if(!strcmp($aliment, $array)){
					$chemin = $tab['chemin']."/".$array;
					echo $chemin."<br/>";
					return;
				}
			}
			else
				$chemin = afficherChemin($array, $aliment);
		}
	}
	}
}

/** Afficher les recettes qui contient de ingredient qu'utilisateur choisi
*@aliment - ingredient
*@listeRec - la liste des recettes dans donnees
@page - la page qu'utilisateur est sur
*/
function afficherRecettes($aliment, $listeRec, $page, $listeAli){
	echo '<div class="row">
			<div class="col-md-12 column">
				<div class="page-header" style="color:#DF744A; text-shadow: 6px 6px 3px #FF9F84;">
					<h1 >'.$aliment.'</h1>
				</div>
				<small style="color:#DF744A;">';
					$tab = ListeNavigateur($listeAli);
					echo afficherChemin($tab, $aliment);
				echo '</small><br/>
				
			</div>
			</div>';
			$i = 0;
			$recettes = chercheRecettes($aliment, $listeRec);
			if(sizeof($recettes) > 0) {
				while($i < sizeof($recettes)){
					if(sizeof($recettes)-$i == 1){
						echo '
						<div class="nav navbar-nav">
						<div class="col-md-4 column">
							';if($i < sizeof($recettes)){ afficherImage($aliment, $recettes[$i], $page); $i++; }echo '
						</div>
						<div class="col-md-4 column">
						</div>
						<div class="col-md-4 column">
						</div>
						</div>';
					}
					else if(sizeof($recettes)-$i == 2){
						echo '
						<div class="nav navbar-nav">
						<div class="col-md-4 column">
							';if($i < sizeof($recettes)){ afficherImage($aliment, $recettes[$i], $page); $i++; }echo '
						</div>
						<div class="col-md-4 column">
							';if($i < sizeof($recettes)){ afficherImage($aliment, $recettes[$i], $page); $i++; }echo '
						</div>
						<div class="col-md-4 column">
						</div>
						</div>';
					}
					else{
						echo '
						<div class="nav navbar-nav" >
						<div class="col-md-4 column">
							';if($i < sizeof($recettes)){ afficherImage($aliment, $recettes[$i], $page); $i++; }echo '
						</div>
						<div class="col-md-4 column">
							';if($i < sizeof($recettes)){ afficherImage($aliment, $recettes[$i], $page); $i++; }echo '
						</div>
						<div class="col-md-4 column">
							'; if($i < sizeof($recettes)){ afficherImage($aliment, $recettes[$i], $page); $i++; }echo '
						</div>
						</div>';
					}
				}
			}else echo "<h4>It can't make any recipes.</h4>";
}


?>