<?php
include("Fonction/donneeTraiter.php");
include("Donnees.inc.php");
	$favorite = array();
	session_start();
	if(isset($_COOKIE["favorite"]))  // si utilisateur a ajoute des recettes preferee avant login
		$favorite = unserialize($_COOKIE['favorite']);

	if(isset($_SESSION['usr'])){
		if($_SESSION['usr']['isLogin'] == true){ // si deja login
			$nom = $_SESSION['usr']['name'];
			if(!isset($_SESSION[$nom]['favorite'])){
				$_SESSION[$nom]['favorite'] = array();
			}
			foreach($favorite as $recette){  // prendre les recettes qui est choisi avant login dans la session
				ajouterFavorite($recette,  $_SESSION[$nom]['favorite']);
			}
			$favorite = $_SESSION[$nom]['favorite'];
		}
	}
	
	
	if(isset($_GET['delfavorite'])){
		if(isset($_COOKIE['favorite'])){
			$favorite = unserialize($_COOKIE['favorite']);
			deleteFavorite($_GET['delfavorite'], $favorite);
			setcookie('favorite', serialize($favorite));
			$_COOKIE['favorite'] = serialize($favorite);
		}
	}
	
?>




<!DOCTYPE html>
<html>
<head>
	<title>Mes recettes préféré</title>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="http://cdn.bootcss.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<link href="http://cdn.bootcss.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/htmleaf-demo.css">
	<link rel="stylesheet" type="text/css" href="css/bootsnav.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<style>
		body
		{
    		margin: 0;
    		background-color: #FEDCD2;
		}
	</style>
</head>

<body>
<div class="demo" style="padding: 2em 0;">
	<div class="container" border-radius:50px;>
		<?php Navigateur($Hierarchie); ?>
	<div class="row">
		<div class="col-md-12 column">
			<div class="page-header">
				<h1 style="color:#DF744A; text-shadow: 6px 6px 3px #FF9F84;">
					Mes recettes préféré <small>suivre ma coeur</small>
				</h1>
			</div>
		</div>
	</div>
	<div class="row">
		<?php $i = 0;
			if(sizeof($favorite) > 0) {
				while($i < sizeof($favorite)){ 
					if(sizeof($favorite) - $i == 2){?>
						<div class="nav navbar-nav">
						<div class="col-md-6 column">
							<h3><?php if($i < sizeof($favorite)){ afficherImage("", strtr($favorite[$i], '_', ' '), "favorite"); $i++; } ?></h3>
						</div>
						<div class="col-md-6 column">
							<h3><?php if($i < sizeof($favorite)){ afficherImage("", strtr($favorite[$i], '_', ' '), "favorite"); $i++; } ?></h3>
						</div>
						</div>
					<?php }else{ ?>
						<div class="nav navbar-nav">
						<div class="col-md-4 column">
							<h3><?php if($i < sizeof($favorite)){ afficherImage("", strtr($favorite[$i], '_', ' '), "favorite"); $i++; } ?></h3>
						</div>
						<div class="col-md-4 column">
							<h3><?php if($i < sizeof($favorite)){ afficherImage("", strtr($favorite[$i], '_', ' '), "favorite"); $i++; } ?></h3>
						</div>
						<div class="col-md-4 column">
							<h3><?php if($i < sizeof($favorite)){ afficherImage("", strtr($favorite[$i], '_', ' '), "favorite"); $i++; } ?></h3>
						</div>
						</div>
			<?php }}}else echo "<h4>Vous n'avez pas de recette préféré</h4>"; ?>
	</div>
	</div>
</div>
	
	
	
	<script src="js/jquery-1.11.0.min.js" type="text/javascript"></script>
	<script src="http://cdn.bootcss.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/bootsnav.js"></script>
</body>






</html>