<!DOCTYPE html>
<?php include ("Donnees.inc.php");?>
<?php include ("Fonction/donneeTraiter.php");?>
<?php 
	session_start();
	$aliment = strtr($_GET['aliment'], "_", " ");
	if(isset($_GET['addfavorite'])){
		if(isset($_COOKIE['favorite'])){
			$favorite = unserialize($_COOKIE['favorite']);
			ajouterFavorite($_GET['addfavorite'], $favorite);
			setcookie('favorite', serialize($favorite));
			$_COOKIE['favorite'] = serialize($favorite);
		}
		else{
			$favorite = array();
			ajouterFavorite($_GET['addfavorite'], $favorite);
			setcookie('favorite', serialize($favorite));
			$_COOKIE['favorite'] = serialize($favorite);
		}
	}
	
?>

<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Aliment</title>
	<link href="http://cdn.bootcss.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<link href="http://cdn.bootcss.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/htmleaf-demo.css">
	<link rel="stylesheet" type="text/css" href="css/bootsnav.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<style>
		body
		{
    		margin: 0;
    		background-color: #FEDCD2;
		}
	</style>
</head>

<body>
    <div class="demo" style="padding: 2em 0;">
        <div class="container">
            <?php Navigateur($Hierarchie) ?>
       		<?php 
				afficherRecettes($aliment, $Recettes, "aliment", $Hierarchie);
			?>
        </div>
    </div>
   
 
	
    <script src="js/jquery-1.11.0.min.js" type="text/javascript"></script>
	<script src="http://cdn.bootcss.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/bootsnav.js"></script>
</body>
</html>